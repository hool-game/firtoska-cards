export abstract class Scoring {
  abstract calculateScore(): number;
}
