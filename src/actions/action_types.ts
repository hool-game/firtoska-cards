/**
 * Action types for all the actions.
 * This is useful for identifying the type of an Action instance.
 */
export enum ActionTypes {
  BID = 'bid',
  BID_WITH_TRUMP = 'bid_with_trump',
  CLAIM = 'claim',
  PLAY = 'play',
  SWAP = 'swap',
  FLIP = 'flip',
  TAKE = 'take',
  CHANGE_TRUMP = 'change_trump',
  DOUBLING_CUBE = 'doubling_cube',
}
