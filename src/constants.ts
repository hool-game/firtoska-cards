/**
 * Possible sides of a game board.
 */
export enum GameSides {
  SOUTH = 0,
  WEST = 1,
  EAST = 2,
  NORTH = 3,
}
